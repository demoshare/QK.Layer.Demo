﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QK.Web.Controllers
{
    public class HomeController : Controller
    {
        public JsonResult Index()
        {
            return Json(new { data = 1 }, JsonRequestBehavior.AllowGet);
        }
    }
}