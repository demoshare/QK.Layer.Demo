﻿using QK.Common;
using QK.Framework;
using QK.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QK.Web.Controllers
{
    public class NavController : BaseController
    {
        private BLL.navigation navService = new BLL.navigation();
        public object GetList(int pid = 0)
        {
            var list = navService.GetList(pid, DTEnums.NavigationEnum.System.ToString());
            if (pid > 0) {
                var tlist = list.Where(p => p.parent_id == pid);
                List<NavList> navList = new List<NavList>();
                foreach (var item in tlist)
                {
                    navList.Add(new NavList() {
                        id = item.id,
                        title=item.title,
                        icon=item.icon_url,
                    });

                }
                List<NavList> resultList = new List<NavList>();
                foreach (var item in navList)
                {
                    var clist = from a in list where a.parent_id == item.id select a ;
                    List<NavChildren> navChildren = new List<NavChildren>();

                    foreach (var citem in clist)
                    {
                        navChildren.Add(new NavChildren() {
                            id = citem.id,
                            title = citem.title,
                            icon = citem.icon_url,
                            url= citem.link_url
                        });
                    }
                    item.children = navChildren;
                    resultList.Add(item);
                }
                return OK(resultList);

            }
            else
                return OK(from a in list where a.parent_id == pid select new { a.id, title = a.sub_title, icon = a.icon_url });

        }

        //private void GetChilds(List<Model.navigation> oldData, List<Model.navigation> newData, int parent_id, int class_layer)
        //{
        //    class_layer++;
        //    DataRow[] dr = oldData.Select("parent_id=" + parent_id);
        //    for (int i = 0; i < dr.Length; i++)
        //    {
        //        DataRow row = newData.NewRow();//创建新行
        //        //循环查找列数量赋值
        //        for (int j = 0; j < dr[i].Table.Columns.Count; j++)
        //        {
        //            row[dr[i].Table.Columns[j].ColumnName] = dr[i][dr[i].Table.Columns[j].ColumnName];
        //        }
        //        row["class_layer"] = class_layer;//赋值深度字段
        //        newData.Rows.Add(row);//添加新行
        //        //调用自身迭代
        //        this.GetChilds(oldData, newData, int.Parse(dr[i]["id"].ToString()), class_layer);
        //    }
        //}

    }
}