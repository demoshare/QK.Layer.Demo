﻿function AjaxHelper() {
    this.ajax = function (url, type, dataType, data, callback) {
        //var newData = [{ "name": "token", "value": "0A54ADF706808CC63D3D47932EEAB6EE" }];
        //$.each(data, function (key, val) {
        //    newData.push({ "name": key, "value": val });
        //});
    
        $.ajax({
            url: url,
            type: type,
            dataType: dataType,
            data: data,
            success: callback,
            error: function (xhr, errorType, error) {
                switch (xhr.status)
                {
                    case 500:
                        alert("系统异常，请稍后再试");
                        break;
                }
            }
        })
    }
}
AjaxHelper.prototype.get = function (url, data, callback) {
    this.ajax(url, 'GET', 'json', data, callback)
}
AjaxHelper.prototype.post = function (url, data, callback) {
    this.ajax(url, 'POST', 'json', data, callback)
}

AjaxHelper.prototype.put = function (url, data, callback) {
    this.ajax(url, 'PUT', 'json', data, callback)
}

AjaxHelper.prototype.delete = function (url, data, callback) {
    this.ajax(url, 'DELETE', 'json', data, callback)
}

AjaxHelper.prototype.jsonp = function (url, data, callback) {
    this.ajax(url, 'GET', 'jsonp', data, callback)
}

AjaxHelper.prototype.constructor = AjaxHelper