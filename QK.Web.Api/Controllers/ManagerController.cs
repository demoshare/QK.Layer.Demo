﻿using QK.Framework;
using QK.Web.Api.ViewModel.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace QK.Web.Api.Controllers
{
    public class ManagerController : BaseController
    {
        private readonly BLL.manager managerService = new BLL.manager();


        /// <summary>
        /// 分页数据列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="sortname"></param>
        /// <param name="sortorder"></param>
        /// <returns></returns>
        public object GetPagedList(int page = 1, int limit = 30, string keyWords = "", string sortname = "", string sortorder = "", string userName = "", string telephone = "")
        {
            page = page <= 0 ? 1 : page;
            limit = limit <= 0 ? 30 : limit;
            string filedOrder = string.Concat(sortname, " ", sortorder);
            if (string.IsNullOrWhiteSpace(filedOrder))
            {
                filedOrder = " id desc";
            }
            int recordCount = 0;
            var list = managerService.GetPagedList(limit, page, keyWords, filedOrder, out recordCount);

            return OK(recordCount, list);
        }


        /// <summary>
        /// 根据编号删除信息
        /// </summary>
        /// <param name="ids">多个用,隔开[英文]</param>
        /// <returns></returns>
        public object Delete(string ids)
        {

            if (string.IsNullOrWhiteSpace(ids))
            {
                return OK(BusinessCode.ParameterError, "ids参数必填");
            }
            bool result = false;
            if (ids.Split(',').Length > 0)
            {
                result = managerService.Delete(Convert.ToInt32(ids));
            }
            else
            {
                result = managerService.Delete(ids);
            }

            if (result)
                return OK(BusinessCode.Normal, "修改成功");
            else
                return OK(BusinessCode.UnifiedError, "修改失败");

        }
    }
}