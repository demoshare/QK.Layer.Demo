﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QK.Web.Api.ViewModel
{
    public class NavList
    {

        public int id { get; set; }
        public string title { get; set; }
        public string icon { get; set; }
        public bool spread { get; set; }
        public List<NavChildren> children { get; set; }
    }

    public class NavChildren
    {
        public int id { get; set; }
        public string title { get; set; }
        public string icon { get; set; }
        public string url { get; set; }
    }
}