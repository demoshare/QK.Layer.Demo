﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QK.Web.Api.ViewModel.Search
{
    public class ManagerField
    {
        private string _user_name;
        private string _telephone;

        public string user_name
        {
            get {
                if (string.IsNullOrWhiteSpace(_user_name))
                    return _user_name;
                if (_user_name.Length > 15)
                    return _user_name.Substring(0, 15);
                else
                    return _user_name;
            }
            set {
               
                    value = _user_name;
            }
        }


        public string telephone
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_telephone))
                return _telephone;
                if (_telephone.Length > 15)
                    return _telephone.Substring(0, 15);
                else
                    return _telephone;
            }
             
            set
            {
                value = _telephone;

            }
        }
    }
}