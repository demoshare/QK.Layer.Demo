﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace QK.Framework
{
    public class BaseController : Controller
    {
        #region 提示信息

        public JsonResult OK(object obj)
        {
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OK(int total, object obj)
        {
            if (total > 0)
                return Json(new { code=0, msg="", data = obj, count = total }, JsonRequestBehavior.AllowGet);
            else
                return Json(@"{""code"":0,""msg""="",""data"":[],""count"":""0""}", JsonRequestBehavior.AllowGet);

        }

        public JsonResult OK(BusinessCode code, object data = null)
        {
            return Json(new JsonMvcResult(code, "", data), JsonRequestBehavior.AllowGet);
        }
        public JsonResult OK(BusinessCode code, string msg = "", object data = null)
        {
            return Json(new JsonMvcResult(code, msg, data), JsonRequestBehavior.AllowGet);
        }

        #endregion



        #region MyRegion

        /// <summary>
        /// 返回JsonResult
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="contentType">内容类型</param>
        /// <param name="contentEncoding">内容编码</param>
        /// <param name="behavior">行为</param>
        /// <returns>JsonReuslt</returns>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResultHelper
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                FormateStr = "yyyy-MM-dd HH:mm:ss"
            };
        }

        /// <summary>
        /// 返回JsonResult.24         /// </summary>
        /// <param name="data">数据</param>
        /// <param name="behavior">行为</param>
        /// <param name="format">json中dateTime类型的格式</param>
        /// <returns>Json</returns>
        protected JsonResult JsonHelper(object data, JsonRequestBehavior behavior, string format)
        {
            return new JsonResultHelper
            {
                Data = data,
                JsonRequestBehavior = behavior,
                FormateStr = format
            };
        }

        /// <summary>
        /// 返回JsonResult42         /// </summary>
        /// <param name="data">数据</param>
        /// <param name="format">数据格式</param>
        /// <returns>Json</returns>
        protected JsonResult JsonHelper(object data, string format)
        {
            return new JsonResultHelper
            {
                Data = data,
                FormateStr = format
            };
        }

        #endregion

    }


}